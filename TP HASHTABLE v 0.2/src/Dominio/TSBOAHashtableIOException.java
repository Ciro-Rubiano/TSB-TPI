package Dominio;

public class TSBOAHashtableIOException  extends  Exception{

    private String message = "Problema al serializar la tabla hash.";

    public TSBOAHashtableIOException()
    {
    }


    public TSBOAHashtableIOException(String msg)
    {
        message = msg;
    }


    @Override
    public String getMessage()
    {
        return message;
    }
}
