package Dominio;

import negocio.TSBOAHashtable;

import java.io.*;

public class TSBOAHashtableReader {

    private String arch = "HashPalabras.dat";


    public TSBOAHashtableReader()
    {
    }


    public TSBOAHashtableReader(String nom)
    {
        arch = nom;
    }



    public TSBOAHashtable read() throws TSBOAHashtableIOException
    {
        TSBOAHashtable sl = null;

        try
        {
            FileInputStream istream = new FileInputStream(arch);
            ObjectInputStream p = new ObjectInputStream(istream);

            sl = ( TSBOAHashtable ) p.readObject();
            System.out.println("Se leyo correctamente");

            p.close();
            istream.close();
        }
        catch (Exception e)
        {
            throw new TSBOAHashtableIOException("No se pudo recuperar la tabla hash.");
        }

        return sl;
    }
}
