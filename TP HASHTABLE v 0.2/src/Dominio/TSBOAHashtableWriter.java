package Dominio;

import negocio.TSBOAHashtable;

import java.io.*;

public class TSBOAHashtableWriter
{

    private String arch = "HashPalabras.dat";

    public TSBOAHashtableWriter() {}


    public TSBOAHashtableWriter(String nom)
    {
        arch = nom;
    }


    public void write (TSBOAHashtable sl) throws TSBOAHashtableIOException
    {
        try
        {
            FileOutputStream ostream = new FileOutputStream(arch);
            ObjectOutputStream p = new ObjectOutputStream(ostream);

            p.writeObject(sl);
            System.out.println("Se grabo correctamente");

            p.flush();
            ostream.close();
        }
        catch ( Exception e )
        {
            throw new TSBOAHashtableIOException("No se pudo grabar la tabla hash.");
        }
    }


}
