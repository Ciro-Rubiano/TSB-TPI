package Dominio;

import negocio.TSBOAHashtable;

import java.util.Map;

public class TablaHashPalabras {

    public negocio.TSBOAHashtable<String, Integer> tabla;

    public TablaHashPalabras()
    {
        this(11);
    }

    public TablaHashPalabras(int initial_capacity)
    {
        tabla = new negocio.TSBOAHashtable<>(initial_capacity);
    }

    public TablaHashPalabras(Map<? extends String, ? extends Integer> t)
    {
     tabla = new negocio.TSBOAHashtable<>(t);
    }


    public int add(String palabra, int count)
    {
        int old;
        if (tabla.containsKey(palabra))
        {
            old = tabla.put(palabra, tabla.get(palabra)+count);
        }
        else
            {
            tabla.put(palabra, count);
            old = 1;
            }


        return old;
    }

    public int remove(String palabra) { return tabla.remove(palabra); }

    public boolean isEmpty() { return tabla.isEmpty(); }

    public int getCount(String palabra) { return tabla.get(palabra); }


    public void grabar()
    {
        try
        {
            TSBOAHashtableWriter slw = new TSBOAHashtableWriter();
            slw.write( this.tabla );
        }
        catch(TSBOAHashtableIOException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void leer()
    {
        try
        {
            TSBOAHashtableReader slr = new TSBOAHashtableReader();
            this.tabla = (TSBOAHashtable<String, Integer>) slr.read();

        }
        catch( TSBOAHashtableIOException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public String toString() { return tabla.toString(); }
}
