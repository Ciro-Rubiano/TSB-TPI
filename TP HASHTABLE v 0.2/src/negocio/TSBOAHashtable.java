package negocio;


import java.io.Serializable;
import java.util.AbstractSet;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;

public class TSBOAHashtable<K,V> implements Map<K,V>, Cloneable, Serializable {

    private final static int MAX_SIZE = Integer.MAX_VALUE;


    // Arreglo de la tabla Hash.
    private Entry<K, V> tabla[];

    // Capacidad inicial del arreglo.
    private int initial_capacity;

    // Cantidad de elementos almacenados.
    private int count;

    //
    private final float load_factor = 0.5f;

    protected transient int modCount;

    private transient Set<K> keySet = null;
    private transient Set<Map.Entry<K,V>> entrySet = null;
    private transient Collection<V> values = null;


    //Constructor Generico.
    public TSBOAHashtable() {
        this(11);
    }

    //Constructor real.
    public TSBOAHashtable(int initial_capacity) {
        if (initial_capacity <= 0) {
            this.initial_capacity = 11;
        } else {
            if (initial_capacity > this.MAX_SIZE) initial_capacity = this.MAX_SIZE;
            else {
                if (!esPrimo(initial_capacity)) this.initial_capacity = siguientePrimo(initial_capacity);
                else this.initial_capacity = initial_capacity;
            }
        }
        this.tabla = new Entry[initial_capacity];
        this.count = 0;
    }


    // Constructo para que la tabla inicie con ciertos elementos, no se como implementarlo.
    public TSBOAHashtable(Map<? extends K, ? extends V> t) {
        this();
        this.putAll(t);
    }


    // Devuelve la cantidad de Elementos almacenados en la tabla.
    @Override
    public int size() {
        return count;
    }


    // Verifica si la tabla esta vacia.
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }


    @Override
    public boolean containsKey(Object key) {
        return this.get((K) key) != null;
    }


    @Override
    public boolean containsValue(Object value) {

        for (Entry<K,V> entry : tabla)
        {
            if (entry != null && entry.getEstado()==1 && value.equals(entry.value)) {
                return true;
            }
        }

        return false;
    }


    @Override
    public V get(Object key) {
        if (key == null) throw new NullPointerException("get(): parámetro null");

        Entry<K, V> unEntry = search_for_entry((K) key);

        if (unEntry != null) {
            return unEntry.getValue();
        } else {
            return null;
        }
    }


    @Override
    public V put(K key, V value) {
        if (key == null || value == null) throw new NullPointerException("put(): parámetro null");

        Entry<K, V> unEntry = search_for_entry(key);
        V old = null;

        if (unEntry != null) {old = unEntry.setValue(value);}
        else
        {
            if (count > (Math.round(tabla.length * load_factor))) //count cantidad de entrys con estado 1
            {
                this.rehash();
            }

            Entry<K, V> nuevo = new Entry<>(key, value);
            int indiceNuevoObjeto = this.calcularIndiceDeEntrada(key);
            tabla[indiceNuevoObjeto] = nuevo;

            count++;
            this.modCount++;
        }
        return old;
    }


    private int calcularIndiceDeEntrada(K key) {
        int indiceNuevoObjeto = h(key);
        int i = indiceNuevoObjeto;
        int j = 1;

        while (!(tabla[i] == null)) {
            i = (indiceNuevoObjeto + j * j) % tabla.length;
            j++;
        }
        return i;
    }


    @Override
    public V remove(Object key) {
        if (key == null) throw new NullPointerException("remove(): parámetro null");

        Entry<K, V> unEntry = search_for_entry((K) key);

        V old = null;

        if (unEntry != null)
        {
            unEntry.setEstado(0);
            this.count--;
            this.modCount++;
            old = unEntry.getValue();
        }
        return old;
    }


    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
        {
            put(e.getKey(), e.getValue());
        }
    }


    @Override
    public void clear() {
        this.tabla = new Entry[initial_capacity];
        this.count = 0;
        this.modCount++;
    }


    @Override
    public Set<K> keySet()
    {
        if(keySet == null)
        {
            //keySet = Collections.synchronizedSet(new KeySet());
            keySet = new KeySet();
        }
        return keySet;
    }


    @Override
    public Collection<V> values()
    {
        if(values==null)
        {
            // values = Collections.synchronizedCollection(new ValueCollection());
            values = new ValueCollection();
        }
        return values;
    }


    @Override
    public Set<Map.Entry<K, V>> entrySet()
    {
        if(entrySet == null)
        {
            // entrySet = Collections.synchronizedSet(new EntrySet());
            entrySet = new EntrySet();
        }
        return entrySet;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        TSBOAHashtable<K, V> t = (TSBOAHashtable<K, V>)super.clone();
        t.tabla = new Entry[tabla.length];
        for (int i = 0; i < tabla.length; i++)
        {
            t.tabla[i] = tabla[i];
        }
        t.keySet = null;
        t.entrySet = null;
        t.values = null;
        t.modCount = 0;
        return t;
    }


    @Override
    public boolean equals(Object obj) { //HAY QUE PROGRAMARLA Esta copiada de la tabla vista en clase
        if(!(obj instanceof Map)) { return false; }

        Map<K, V> t = (Map<K, V>) obj;
        if(t.size() != this.size()) { return false; }

        try
        {
            Iterator<Map.Entry<K,V>> i = this.entrySet().iterator();
            while(i.hasNext())
            {
                Map.Entry<K, V> e = i.next();
                K key = e.getKey();
                V value = e.getValue();

                V aux = t.get(key);
                if(aux == null || !value.equals(aux)) { return false; }
            }
        }

        catch (ClassCastException | NullPointerException e)
        {
            return false;
        }
        return true;
    }


    @Override
    public int hashCode()
    {
        if (this.isEmpty()) return 0;

        int hc = 0;
        for (Map.Entry<K, V> entry : this.entrySet()) {
            hc += entry.hashCode();
        }
        return hc;
    }


    @Override
    public String toString() {
        String st = "";
        for (Entry<K, V> entry : tabla) {
            if (entry != null && entry.getEstado() == 1) {
                st += entry.toString() + "\n";
            }
        }
        return st;
    }


    protected void rehash()
    {
        int viejo_length = this.tabla.length;
        int nuevo_length = this.siguienteTamaño(viejo_length);

        if (nuevo_length > MAX_SIZE) nuevo_length = MAX_SIZE;

        Entry<K, V>[] viejaTabla = tabla;

        tabla= new Entry[nuevo_length];
        count = 0;
        this.modCount++;

        for (Entry<K,V> entry : viejaTabla) {
            if (entry != null) //unicamente los cerrados, los tumba y abiertos no necesitan entrar en la nueva tabla
            {
                this.put(entry.key,entry.value); //calcula el nuevo indice para la nueva tabla, y guarda el objeto Entry tabla[i]
            }
        }
    }


    private boolean esPrimo(int n) {

        if (n % 2 == 0) return false;

        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }


    private int siguienteTamaño(int anterior) {
        int nuevoTamaño = anterior * 2 - 1;
        return siguientePrimo(nuevoTamaño);
    }


    private int siguientePrimo(int n) {
        if (n % 2 == 0) {
            n += 1;
        } //hace falta por si ingresa un numero par, sino el ciclo siguiente es infinito

        while (!esPrimo(n)) {
            n += 2;
        }

        return n;
    }


    private int h(int k) {
        return h(k, this.tabla.length);
    }


    private int h(K key) {
        return h(key.hashCode(), this.tabla.length);
    }


    private int h(K key, int t) {
        return h(key.hashCode(), t);
    }


    private int h(int k, int t) {
        return Math.abs(k) % t;
    }


    private Entry<K, V> search_for_entry(K key) {

        int indice = h(key);
        int i = indice;
        int j = 1; //termino cuadratico j^2

        while (tabla[i] != null && tabla[i].getEstado() != 0)
        {
            if (key.equals(tabla[i].getKey())) //si es la clave buscada
            {
                return tabla[i];
            }
            i = (indice + j * j) % tabla.length; //Exploracion cuadratica circular
            j++;
        }
        return null;
    }


    private int search_for_index(K key) {
        int indice = h(key);
        int i = indice;
        int j = 1; //termino cuadratico j^2

        while (tabla[i] != null) //mientras el casillero de la posicion actual este cerrado o tumba
        {
            if (key.equals(tabla[i].getKey())) //si es la clave buscada
            {
                return i;
            }
            i = (indice + j * j) % tabla.length; //Exploracion cuadratica circular
            j++;
        }

        return -1;
    }


    private class Entry<K,V> implements Map.Entry<K,V>
    {
        private K key;
        private V value;
        private int estado;
        //null abierto
        //0 muerto
        //1 cerrado

        public Entry(K key, V value)
        {
            if(key==null || value==null) throw new IllegalArgumentException("Entry(): parametro null");

            this.key = key;
            this.value = value;
            this.estado = 1;
        }

        /**
         * Returns the key corresponding to this entry.
         *
         * @return the key corresponding to this entry
         * @throws IllegalStateException implementations may, but are not
         *                               required to, throw this exception if the entry has been
         *                               removed from the backing map.
         */
        @Override
        public K getKey() {
            return key;
        }

        /**
         * Returns the value corresponding to this entry.  If the mapping
         * has been removed from the backing map (by the iterator's
         * {@code remove} operation), the results of this call are undefined.
         *
         * @return the value corresponding to this entry
         * @throws IllegalStateException implementations may, but are not
         *                               required to, throw this exception if the entry has been
         *                               removed from the backing map.
         */
        @Override
        public V getValue() {
            return value;
        }


        public int getEstado(){
            return estado;
        }

        public void setEstado(int e){
            if(e>=0) estado = e;
        }

        /**
         * Replaces the value corresponding to this entry with the specified
         * value (optional operation).  (Writes through to the map.)  The
         * behavior of this call is undefined if the mapping has already been
         * removed from the map (by the iterator's {@code remove} operation).
         *
         * @param value new value to be stored in this entry
         * @return old value corresponding to the entry
         * @throws UnsupportedOperationException if the {@code put} operation
         *                                       is not supported by the backing map
         * @throws ClassCastException            if the class of the specified value
         *                                       prevents it from being stored in the backing map
         * @throws NullPointerException          if the backing map does not permit
         *                                       null values, and the specified value is null
         * @throws IllegalArgumentException      if some property of this value
         *                                       prevents it from being stored in the backing map
         * @throws IllegalStateException         implementations may, but are not
         *                                       required to, throw this exception if the entry has been
         *                                       removed from the backing map.
         */
        @Override
        public V setValue(V value)
        {
            if(value==null) throw new IllegalArgumentException("setValue(): Parametro null");

            V old = this.value;
            this.value = value;
            return old;
        }

        public int hashCode() {
            int hash = 7;
            hash = 61 * hash + Objects.hashCode(key);
            hash = 61 * hash + Objects.hashCode(value);
            return hash;
        }

        public boolean equals(Object o) {
            if (o == null) return false;
            if (this == o) return true;
            if (this.getClass() != o.getClass()) return false;


            Entry aux = (Entry) o;
            if (!Objects.equals(this.key, aux.key)) return false;
            if (!Objects.equals(this.value, aux.value)) return false;

            return true;
        }

        @Override
        public String toString() {
            return "(" + this.key + ";" + this.value + ")";
        }
    }

    private class KeySet extends AbstractSet<K> {

        /**
         * Returns an iterator over the elements contained in this collection.
         *
         * @return an iterator over the elements contained in this collection
         */
        @Override
        public Iterator<K> iterator() {return new KeySetIterator();}

        @Override
        public int size() {
            return TSBOAHashtable.this.count;
        }

        /**
         * {@inheritDoc}
         *
         * @param o
         * @throws ClassCastException   {@inheritDoc}
         * @throws NullPointerException {@inheritDoc}
         * @implSpec This implementation iterates over the elements in the collection,
         * checking each element in turn for equality with the specified element.
         */
        @Override
        public boolean contains(Object o) {return TSBOAHashtable.this.containsKey(o);}

        /**
         * {@inheritDoc}
         *
         * @param o
         * @throws UnsupportedOperationException {@inheritDoc}
         * @throws ClassCastException            {@inheritDoc}
         * @throws NullPointerException          {@inheritDoc}
         * @implSpec This implementation iterates over the collection looking for the
         * specified element.  If it finds the element, it removes the element
         * from the collection using the iterator's remove method.
         *
         * <p>Note that this implementation throws an
         * {@code UnsupportedOperationException} if the iterator returned by this
         * collection's iterator method does not implement the {@code remove}
         * method and this collection contains the specified object.
         */
        @Override
        public boolean remove(Object o) {return (TSBOAHashtable.this.remove(o) != null);}

        /**
         * {@inheritDoc}
         *
         * @throws UnsupportedOperationException {@inheritDoc}
         * @implSpec This implementation iterates over this collection, removing each
         * element using the {@code Iterator.remove} operation.  Most
         * implementations will probably choose to override this method for
         * efficiency.
         *
         * <p>Note that this implementation will throw an
         * {@code UnsupportedOperationException} if the iterator returned by this
         * collection's {@code iterator} method does not implement the
         * {@code remove} method and this collection is non-empty.
         */
        @Override
        public void clear() {TSBOAHashtable.this.clear();}

        private class KeySetIterator implements Iterator<K> {
            private int currentEntry;
            private boolean nextOk;
            private int expected_modCount;

            public KeySetIterator() {
                currentEntry = -1;
                nextOk = false;
                expected_modCount = TSBOAHashtable.this.modCount;
            }

            /**
             * Returns {@code true} if the iteration has more elements.
             * (In other words, returns {@code true} if {@link #next} would
             * return an element rather than throwing an exception.)
             *
             * @return {@code true} if the iteration has more elements
             */
            @Override
            public boolean hasNext() {

                Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                if (isEmpty()) return false;

                if (currentEntry >= t.length - 1) return false;

                int nextEntry = currentEntry + 1;
                while (nextEntry < t.length && (t[nextEntry] == null || t[nextEntry].getEstado()==0)) {
                    nextEntry++;
                }
                if (nextEntry >= t.length) return false;

                return true;
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             * @throws NoSuchElementException if the iteration has no more elements
             */
            @Override
            public K next() {
                if (modCount != expected_modCount) throw new ConcurrentModificationException("next(): modificacion inesperada");

                if (!hasNext()) throw new NoSuchElementException("next(): no quedan elementos");

                Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                currentEntry++;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry++;

                nextOk = true;
                return t[currentEntry].getKey();
            }

            /**
             * Removes from the underlying collection the last element returned
             * by this iterator (optional operation).  This method can be called
             * only once per call to {@link #next}.
             * <p>
             * The behavior of an iterator is unspecified if the underlying collection
             * is modified while the iteration is in progress in any way other than by
             * calling this method, unless an overriding class has specified a
             * concurrent modification policy.
             * <p>
             * The behavior of an iterator is unspecified if this method is called
             * after a call to the {@link #forEachRemaining forEachRemaining} method.
             *
             * @throws UnsupportedOperationException if the {@code remove}
             *                                       operation is not supported by this iterator
             * @throws IllegalStateException         if the {@code next} method has not
             *                                       yet been called, or the {@code remove} method has already
             *                                       been called after the last call to the {@code next}
             *                                       method
             * @implSpec The default implementation throws an instance of
             * {@link UnsupportedOperationException} and performs no other action.
             */
            @Override
            public void remove()
            {
                if(!nextOk) throw new IllegalStateException("remove(): debe invocar next() antes de remove()");

                V basura = TSBOAHashtable.this.remove(currentEntry);

                Entry<K,V> t[] = TSBOAHashtable.this.tabla;

                currentEntry--;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry--;

                nextOk = false;

                TSBOAHashtable.this.count--;
                TSBOAHashtable.this.modCount++;
                expected_modCount++;

            }
        }
    }

    private class ValueCollection extends AbstractCollection<V> {
        /**
         * Returns an iterator over the elements contained in this collection.
         *
         * @return an iterator over the elements contained in this collection
         */
        @Override
        public Iterator<V> iterator() {
            return new ValueCollectionIterator();
        }

        @Override
        public int size() {
            return TSBOAHashtable.this.count;
        }

        /**
         * {@inheritDoc}
         *
         * @throws UnsupportedOperationException {@inheritDoc}
         * @implSpec This implementation iterates over this collection, removing each
         * element using the {@code Iterator.remove} operation.  Most
         * implementations will probably choose to override this method for
         * efficiency.
         *
         * <p>Note that this implementation will throw an
         * {@code UnsupportedOperationException} if the iterator returned by this
         * collection's {@code iterator} method does not implement the
         * {@code remove} method and this collection is non-empty.
         */
        @Override
        public void clear() {TSBOAHashtable.this.clear();}

        /**
         * {@inheritDoc}
         *
         * @param o
         * @throws ClassCastException   {@inheritDoc}
         * @throws NullPointerException {@inheritDoc}
         * @implSpec This implementation iterates over the elements in the collection,
         * checking each element in turn for equality with the specified element.
         */
        @Override
        public boolean contains(Object o) {return TSBOAHashtable.this.containsValue(o);}

        private class ValueCollectionIterator implements Iterator<V>
        {
            private int currentEntry;
            private boolean nextOk;
            private int expected_modCount;

            public ValueCollectionIterator()
            {
                currentEntry = -1;
                nextOk = false;
                expected_modCount = TSBOAHashtable.this.modCount;
            }

            /**
             * Returns {@code true} if the iteration has more elements.
             * (In other words, returns {@code true} if {@link #next} would
             * return an element rather than throwing an exception.)
             *
             * @return {@code true} if the iteration has more elements
             */
            @Override
            public boolean hasNext()
            {
                currentEntry = -1;
                nextOk = false;
                expected_modCount = TSBOAHashtable.this.modCount;Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                if (isEmpty()) return false;

                if (currentEntry >= t.length - 1) return false;

                int nextEntry = currentEntry + 1;
                while (nextEntry < t.length && (t[nextEntry] == null || t[nextEntry].getEstado()==0)) {
                    nextEntry++;
                }
                if (nextEntry >= t.length) return false;

                return true;
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             * @throws NoSuchElementException if the iteration has no more elements
             */
            @Override
            public V next()
            {
                if (modCount != expected_modCount) throw new ConcurrentModificationException("next(): modificacion inesperada");

                if (!hasNext()) throw new NoSuchElementException("next(): no quedan elementos");

                Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                currentEntry++;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry++;

                nextOk = true;
                return t[currentEntry].getValue();
            }

            /**
             * Removes from the underlying collection the last element returned
             * by this iterator (optional operation).  This method can be called
             * only once per call to {@link #next}.
             * <p>
             * The behavior of an iterator is unspecified if the underlying collection
             * is modified while the iteration is in progress in any way other than by
             * calling this method, unless an overriding class has specified a
             * concurrent modification policy.
             * <p>
             * The behavior of an iterator is unspecified if this method is called
             * after a call to the {@link #forEachRemaining forEachRemaining} method.
             *
             * @throws UnsupportedOperationException if the {@code remove}
             *                                       operation is not supported by this iterator
             * @throws IllegalStateException         if the {@code next} method has not
             *                                       yet been called, or the {@code remove} method has already
             *                                       been called after the last call to the {@code next}
             *                                       method
             * @implSpec The default implementation throws an instance of
             * {@link UnsupportedOperationException} and performs no other action.
             */
            @Override
            public void remove()
            {
                if(!nextOk) throw new IllegalStateException("remove(): debe invocar next() antes de remove()");

                V basura = TSBOAHashtable.this.remove(currentEntry);

                Entry<K,V> t[] = TSBOAHashtable.this.tabla;

                currentEntry--;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry--;

                nextOk = false;

                TSBOAHashtable.this.count--;
                TSBOAHashtable.this.modCount++;
                expected_modCount++;
            }
        }
    }

    private class EntrySet extends AbstractSet<Map.Entry<K, V>>
    {

        /**
         * Returns an iterator over the elements contained in this collection.
         *
         * @return an iterator over the elements contained in this collection
         */
        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntrySetIterator();
        }

        @Override
        public int size() {
            return TSBOAHashtable.this.count;
        }

        /**
         * {@inheritDoc}
         *
         * @throws UnsupportedOperationException {@inheritDoc}
         * @implSpec This implementation iterates over this collection, removing each
         * element using the {@code Iterator.remove} operation.  Most
         * implementations will probably choose to override this method for
         * efficiency.
         *
         * <p>Note that this implementation will throw an
         * {@code UnsupportedOperationException} if the iterator returned by this
         * collection's {@code iterator} method does not implement the
         * {@code remove} method and this collection is non-empty.
         */
        @Override
        public void clear() {TSBOAHashtable.this.clear();}

        /**
         * {@inheritDoc}
         *
         * @param o
         * @throws ClassCastException   {@inheritDoc}
         * @throws NullPointerException {@inheritDoc}
         * @implSpec This implementation iterates over the elements in the collection,
         * checking each element in turn for equality with the specified element.
         */
        @Override
        public boolean contains(Object o)
        {
            if(o == null) return false;
            if(!(o instanceof Entry)) return false;

            Entry<K,V> obEntry = (Entry<K,V>)o;
            K key = obEntry.getKey();
            V value = obEntry.getValue();

            if(TSBOAHashtable.this.containsKey(key) && TSBOAHashtable.this.containsValue(value)) return true;

            return false;
        }

        /**
         * {@inheritDoc}
         *
         * @param o
         * @throws UnsupportedOperationException {@inheritDoc}
         * @throws ClassCastException            {@inheritDoc}
         * @throws NullPointerException          {@inheritDoc}
         * @implSpec This implementation iterates over the collection looking for the
         * specified element.  If it finds the element, it removes the element
         * from the collection using the iterator's remove method.
         *
         * <p>Note that this implementation throws an
         * {@code UnsupportedOperationException} if the iterator returned by this
         * collection's iterator method does not implement the {@code remove}
         * method and this collection contains the specified object.
         */
        @Override
        public boolean remove(Object o) {
            if (!contains(o)) return false;

            Entry<K,V> obEntry = (Entry<K,V>)o;
            K key = obEntry.getKey();

            V basura = TSBOAHashtable.this.remove(key);

            TSBOAHashtable.this.count--;
            TSBOAHashtable.this.modCount++;
            return true;
        }

        private class EntrySetIterator implements Iterator<Map.Entry<K,V>>
        {
            private int currentEntry;
            private boolean nextOk;
            private int expected_modCount;

            public EntrySetIterator()
            {
                currentEntry = -1;
                nextOk = false;
                expected_modCount = TSBOAHashtable.this.modCount;
            }

            /**
             * Returns {@code true} if the iteration has more elements.
             * (In other words, returns {@code true} if {@link #next} would
             * return an element rather than throwing an exception.)
             *
             * @return {@code true} if the iteration has more elements
             */
            @Override
            public boolean hasNext()
            {
                Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                if (isEmpty()) return false;

                if (currentEntry >= t.length - 1) return false;

                int nextEntry = currentEntry + 1;
                while (nextEntry < t.length && (t[nextEntry] == null || t[nextEntry].getEstado()==0)) {
                    nextEntry++;
                }
                if (nextEntry >= t.length) return false;

                return true;
            }

            /**
             * Returns the next element in the iteration.
             *
             * @return the next element in the iteration
             * @throws NoSuchElementException if the iteration has no more elements
             */
            @Override
            public Entry<K, V> next()
            {
                if (modCount != expected_modCount) throw new ConcurrentModificationException("next(): modificacion inesperada");

                if (!hasNext()) throw new NoSuchElementException("next(): no quedan elementos");

                Entry<K, V> t[] = TSBOAHashtable.this.tabla;

                currentEntry++;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry++;

                nextOk = true;
                return t[currentEntry];
            }

            /**
             * Removes from the underlying collection the last element returned
             * by this iterator (optional operation).  This method can be called
             * only once per call to {@link #next}.
             * <p>
             * The behavior of an iterator is unspecified if the underlying collection
             * is modified while the iteration is in progress in any way other than by
             * calling this method, unless an overriding class has specified a
             * concurrent modification policy.
             * <p>
             * The behavior of an iterator is unspecified if this method is called
             * after a call to the {@link #forEachRemaining forEachRemaining} method.
             *
             * @throws UnsupportedOperationException if the {@code remove}
             *                                       operation is not supported by this iterator
             * @throws IllegalStateException         if the {@code next} method has not
             *                                       yet been called, or the {@code remove} method has already
             *                                       been called after the last call to the {@code next}
             *                                       method
             * @implSpec The default implementation throws an instance of
             * {@link UnsupportedOperationException} and performs no other action.
             */
            @Override
            public void remove()
            {
                if(!nextOk) throw new IllegalStateException("remove(): debe invocar next() antes de remove()");

                V basura = TSBOAHashtable.this.remove(currentEntry);

                Entry<K,V> t[] = TSBOAHashtable.this.tabla;

                currentEntry--;
                while (t[currentEntry] == null || t[currentEntry].getEstado()==0) currentEntry--;

                nextOk = false;

                TSBOAHashtable.this.count--;
                TSBOAHashtable.this.modCount++;
                expected_modCount++;
            }
        }
    }
}
