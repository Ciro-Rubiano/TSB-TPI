package negocio;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class TSBHashTableDAbierto<K,V> implements Map<K,V>, Cloneable, Serializable
{

    private final static int MAX_SIZE = Integer.MAX_VALUE;


    // Arreglo de la tabla Hash.
    private Entry<K,V> tabla [];

    // Capacidad inicial del arreglo.
    private int initial_capacity;

    // Cantidad de elementos almacenados.
    private int count;

    //
    private final float load_factor = 0.5f;

    protected transient int modCount;

    //Constructor Generico.
    public TSBHashTableDAbierto() { this(11); }


    //Constructor real.
    public TSBHashTableDAbierto(int initial_capacity)
    {
        if(initial_capacity <= 0) { initial_capacity = 11; }
        else
        {
            if(initial_capacity > TSBHashTableDAbierto.MAX_SIZE)
            {
                initial_capacity = TSBHashTableDAbierto.MAX_SIZE;
            }
            else
            {
                if(!esPrimo(initial_capacity)) {initial_capacity = siguientePrimo(initial_capacity);}
            }
        }

        this.tabla = new Entry [initial_capacity];
        this.initial_capacity = initial_capacity;
        this.count = 0;
    }



    // Constructo para que la tabla inicie con ciertos elementos, no se como implementarlo.
    public TSBHashTableDAbierto(Map<? extends K,? extends V> t)
    {
        this.putAll(t);
    }


    // Devuelve la cantidad de Elementos almacenados en la tabla.
    @Override
    public int size()
    {
        return count;
    }


    // Verifica si la tabla esta vacia.
    @Override
    public boolean isEmpty()
    {
        return this.count==0;
    }


    @Override
    public boolean containsKey(Object key)
    {
        return this.get((K)key) != null;
    }


    @Override
    public boolean containsValue(Object value)
    {

        for(int i=0; i<tabla.length; i++)
        {
            if (tabla[i].value.equals(value))
            {
                return true;
            }
        }

        return false;
    }


    @Override
    public V get(Object key)
    {
        if(key == null) throw new NullPointerException("get(): parámetro null");

        Map.Entry<K,V> unEntry = search_for_entry((K) key);

        if (unEntry != null)
        {
            return unEntry.getValue();
        }
        else
        {
            return null;
        }
    }


    @Override
    public V put(K key, V value)
    {
        if(key == null || value == null) throw new NullPointerException("put(): parámetro null");

        Map.Entry<K,V> unEntry = search_for_entry(key);
        V old = null;

        if(unEntry != null)
        {
            old = unEntry.setValue(value);
        }
        else
        {
            Entry<K, V> nuevo = new Entry<>(key, value);

            if (count >= Math.round((tabla.length*load_factor))) //count cantidad de entrys con estado 1
            {
                this.rehash();
            }

            int indiceNuevoObjeto = this.calcularIndiceDeEntrada(key);

            tabla[indiceNuevoObjeto] = nuevo;

            count++;
            this.modCount++;
        }

        return old;

    }


    private int calcularIndiceDeEntrada(K key)
    {
        int indiceNuevoObjeto = h(key);
        int i = indiceNuevoObjeto;
        int j= 1;

        while (tabla[i].getEstado()==1) //si esta cerrado (no esta vacio o no esta tumba) se mete el objeto en esa posicion
        {
            i = indiceNuevoObjeto + j*j;
        }

        return i;
    }


    @Override
    public V remove(Object key)
    {
        if(key == null) throw new NullPointerException("remove(): parámetro null");

        Map.Entry<K, V> objeto = search_for_entry((K)key);

        V old = null;

        if(objeto != null)
        {
            Entry<K,V> objetoConcreto = (Entry<K,V>) objeto;
            objetoConcreto.setEstado(2);
            this.count--;
            this.modCount++;
            old = objetoConcreto.getValue();
        }

        return old;
    }


    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        for(Map.Entry<? extends K, ? extends V> e : m.entrySet())
        {
            put(e.getKey(), e.getValue());
        }
    }


    @Override
    public void clear()
    {
        this.tabla = new Entry[initial_capacity];
        this.count = 0;
        this.modCount++;
    }


    @Override
    public Set<K> keySet()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    @Override
    public Collection<V> values()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    @Override
    public Set<Map.Entry<K, V>> entrySet()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public boolean equals(Object obj)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    @Override
    public int hashCode()
    {
        if(this.isEmpty()) {return 0;}

        int hc = 0;
        for(Map.Entry<K, V> entry : this.entrySet())
        {
            hc += entry.hashCode();
        }

        return hc;
    }


    @Override
    public String toString()
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    protected void rehash()
    {
        int viejo_length = this.tabla.length;
        int nuevo_length = this.siguienteTamaño(viejo_length);

        if (nuevo_length>MAX_SIZE)
        {
            nuevo_length = MAX_SIZE;
        }

        Entry<K, V>[] nuevaTabla = new Entry [nuevo_length];
        Entry<K, V>[] viejaTabla = this.tabla;
        this.tabla = nuevaTabla;

        this.modCount++;

        for(int i=0; i<tabla.length; i++)
        {
            if (tabla[i].getEstado() == 1) //unicamente los cerrados, los tumba y abiertos no necesitan entrar en la nueva tabla
            {
                this.put(viejaTabla[i].getKey(), viejaTabla[i].getValue());
            }
        }


    }



    private boolean esPrimo(int n) {

        if (n%2==0) return false;

        for(int i=3;i*i<=n;i+=2)
        {
            if(n%i==0)
            {
                return false;
            }
        }
        return true;
    }


    private int siguienteTamaño(int anterior)
    {
        int nuevoTamaño = anterior*2-1;
        return siguientePrimo(nuevoTamaño);
    }


    private int siguientePrimo(int n)
    {
        if(n%2==0){n+=1;} //hace falta por si ingresa un numero par, sino el ciclo siguiente es infinito

        while (!esPrimo(n))
        {
            n += 2;
        }

        return  n;
    }


    private int h(int k)
    {
        return h(k, this.tabla.length);
    }


    private int h(K key)
    {
        return h(key.hashCode(), tabla.length);
    }


    private int h(K key, int t)
    {
        return h(key.hashCode(), t);
    }


    private int h(int k, int t)
    {
        return Math.abs(k)%t;
    }



    private Map.Entry<K, V> search_for_entry(K key)
    {
        int indice = h(key.hashCode());
        int i = indice;
        int j=1; //termino cuadratico j^2

        while(tabla[i].getEstado() != 0 )
        {
            if(tabla[i].getKey() == key) //si es la clave buscada
            {
                return tabla[i];
            }
            i = (indice + j*j)%tabla.length; //Exploracion cuadratica circular
            j ++;
        }

        return null;
    }


    private int search_for_index(K key)
    {
        int indice = h(key);
        int i = indice;
        int j=1; //termino cuadratico j^2

        while(tabla[i].getEstado() != 0 ) //mientras el casillero de la posicion actual este cerrado o tumba
        {
            if(tabla[i].getKey() == key) //si es la clave buscada
            {
                return i;
            }

            i = (indice + j*j)%tabla.length; //Exploracion cuadratica circular
            j ++;
        }

        return -1;
    }


    private class Entry<K, V> implements Map.Entry<K, V>
    {
        private K key;
        private V value;
        private int estado;
        //0 abierto
        //1 cerrado
        //2 tumba

        public Entry(K key, V value)
        {
            if(key == null || value == null)
            {
                throw new IllegalArgumentException("Entry(): parámetro null...");
            }

            this.key = key;
            this.value = value;
            this.estado = 0;
        }

        @Override
        public K getKey() {
            return null;
        }

        @Override
        public V getValue() {
            return null;
        }

        @Override
        public V setValue(V value)
        {
            V old = this.value;

            this.value = value;

            return old;
        }

        public int getEstado(){return estado;}

        public void setEstado(int unEstado)
        {
            this.estado = unEstado;
        }

    }

}